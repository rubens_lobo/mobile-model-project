#Contruir imagem
#    docker build -t <IMAGE-NAME> -f ./Dockerfile .
#Rodar os testes
#    docker run --network="host" -v "$PWD/target:/usr/target" <IMAGE-NAME> mvn clean test "-Dcucumber.options=--tags @android" -Denv=des -Dplatform=android -Dmode=remote
FROM maven:3.5.3-jdk-8-alpine
WORKDIR /usr
COPY . /usr
RUN mvn dependency:go-offline -B