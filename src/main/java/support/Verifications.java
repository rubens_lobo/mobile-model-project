package support;

import support.driver.ContextManager;
import io.appium.java_client.MobileElement;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


@Log4j2
public class Verifications extends ContextManager {


    private Verifications() {
        throw new IllegalStateException();
    }


    /**
     * Waits for a defined period
     *
     * @param seconds the number of seconds the function will wait for
     * @author Rubens Lobo
     */
    public static void wait(int seconds) {
        try {
            Thread.sleep(seconds * 1000L);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Verifies if an element is visible and Clickable
     *
     * @return checking an element is visible and can be clicked
     * @author Rubens Lobo
     */
    public static WebElement verifyElementIsClickable(MobileElement element) {
        log.info(String.format("Verificando se o elemento via  %s e visivel e clicavel", element.toString()));
        return new WebDriverWait(getDriver(),configuration.timeout())
                .until(ExpectedConditions.elementToBeClickable(element));

    }

    /**
     * Verifies if the given element is visible.
     *
     * @author Rubens Lobo
     */
    public static MobileElement verifyElementIsVisible(MobileElement element) {
        log.info(String.format("Verificando o elemento via  %s e visivel", element.toString()));
        return (MobileElement) new WebDriverWait(getDriver(),configuration.timeout())
                .until(ExpectedConditions.visibilityOf(element));
    }
}
