
package support.driver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.collect.ImmutableMap;
import lombok.extern.log4j.Log4j2;
import support.config.Configuration;
import io.appium.java_client.AppiumDriver;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebElement;
import support.report.Report;
import cucumber.api.Scenario;
import org.aeonbits.owner.ConfigCache;

import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;


@Log4j2
public class ContextManager {

    @Getter @Setter
    private static Scenario scenario;
    @Getter @Setter
    private static AppiumDriver<WebElement> driver;
    @Getter @Setter
    private static String platform;
    public static final Configuration configuration = ConfigCache.getOrCreate(Configuration.class);

    public static Map<String , Object> getCapabilities(){
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.findAndRegisterModules();
        Map<String , Object> yamlMaps = null;
        try {
            yamlMaps = (LinkedHashMap<String, Object>)
                    mapper.readValue(new FileReader("./src/main/resources/platform/"+getPlatform()+".yml"), Map.class)
                            .get(System.getProperty("mode"));
        } catch (IOException e) {
            log.error(e);
        }
        return yamlMaps;
    }

    public static void quit() {
        if (getScenario().isFailed()) {
            Report.takeScreenShot();
        }
        getDriver().quit();
    }
}
