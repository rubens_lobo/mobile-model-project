package support.driver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import io.appium.java_client.AppiumDriver;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

@Log4j2
public class AppiumDriverSession extends ContextManager {

   public void createInstance() {
       DesiredCapabilities capabilities = new DesiredCapabilities();
       try {
           Map<String , Object> yamlMaps = getCapabilities();
           log.info("Loading capabilities");
           for (Map.Entry<String, Object> entry : yamlMaps.entrySet()) {
               capabilities.setCapability(entry.getKey(), entry.getValue());
           }
           if(yamlMaps.get("serverUrl") != null){
               setDriver(new AppiumDriver<>(new URL((String) yamlMaps.get("serverUrl")),capabilities));
           }else {
               setDriver(new AppiumDriver<>(capabilities));
           }
       } catch (IOException e) {
           throw new IllegalStateException("Unexpected value: " + e.getMessage());
       }
   }
}
