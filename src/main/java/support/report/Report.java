package support.report;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import support.driver.ContextManager;
import io.qameta.allure.Attachment;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.util.Map;


@Log4j2
public class Report extends ContextManager {

    @Attachment(value = "Page Screenshot", type = "image/png")
    public static byte[] takeScreenShot() {
        log.info("Print screen da tela");
        return ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
    }
}