package support.config;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:conf/conf.properties"})
public interface Configuration extends Config {


    @Key("timeout")
    int timeout();

    @Key("faker.locale")
    String faker();
}
