package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.support.PageFactory;
import support.Verifications;
import support.driver.ContextManager;
import support.report.Report;

@Log4j2
public class AlarmPage extends ContextManager {

    @AndroidFindBy(accessibility = "App")
    private MobileElement btnApp;

    @AndroidFindBy(accessibility = "Alarm")
    private MobileElement btnAlarm;

    @AndroidFindBy(accessibility = "Alarm Controller")
    private MobileElement btnAlarmController;

    @AndroidFindBy(id = "io.appium.android.apis:id/one_shot")
    private MobileElement btnOneShot;

    @AndroidFindBy(xpath = "//android.widget.Toast[1]")
    private MobileElement lblMsg;

    public AlarmPage() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver()), this);
    }

    public void acessaOpcaoApp(){
        log.info("Acessa opção App");
        Report.takeScreenShot();
        Verifications.verifyElementIsClickable(btnApp).click();
    }

    public void acessaOpcaoAlarm(){
        log.info("Acessa opção Alarm");
        Verifications.verifyElementIsClickable(btnAlarm).click();
    }

    public void acessaOpcaoAlarmController(){
        log.info("Acessa opção Alarm Controller");
        Verifications.verifyElementIsClickable(btnAlarmController).click();
    }

    public void clickOneShot(){
        log.info("Acessa opção Alarm Controller");
        Report.takeScreenShot();
        Verifications.verifyElementIsClickable(btnOneShot).click();
    }

    public boolean ckeckToastMsg(String value){
        Report.takeScreenShot();
       if(lblMsg.getText().equals(value)){
           return true;
       }else {
           return false;
       }
    }


}
