package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.support.PageFactory;
import support.Verifications;
import support.driver.ContextManager;
import support.report.Report;

@Log4j2
public class DemoIosPage extends ContextManager {
    @iOSXCUITFindBy(id = "IntegerA")
    public MobileElement txtValorA;

    @iOSXCUITFindBy(id = "ComputeSumButton")
    public MobileElement btnCalcula;

    @iOSXCUITFindBy(id = "IntegerB")
    public MobileElement txtValorB;

    @iOSXCUITFindBy(id = "Answer")
    public MobileElement lblAnswer;

    public DemoIosPage() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver()), this);
    }

    public void somar(String valor1, String valor2){
        Verifications.verifyElementIsClickable(txtValorA);
        Report.takeScreenShot();
        txtValorA.sendKeys(valor1);
        txtValorB.sendKeys(valor2);
        btnCalcula.click();
    }

    public String verificaSoma(){
        Verifications.wait(1);
        Report.takeScreenShot();
        return lblAnswer.getText();
    }
}
