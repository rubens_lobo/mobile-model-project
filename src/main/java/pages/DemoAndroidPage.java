package pages;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.core.util.Assert;
import support.driver.ContextManager;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import org.openqa.selenium.support.PageFactory;
import support.report.Report;
import support.Verifications;

@Log4j2
public class DemoAndroidPage extends ContextManager {

    @AndroidFindBy(accessibility = "App")
    public MobileElement btnApp;

    @AndroidFindBy(accessibility = "Alarm")
    public MobileElement btnAlarm;

    @AndroidFindBy(accessibility = "Alarm Controller")
    public MobileElement btnAlarmController;

    @AndroidFindBy(accessibility = "Alarm Service")
    public MobileElement btnAlarmService;

    @AndroidFindBy(id = "io.appium.android.apis:id/start_alarm")
    public MobileElement btnStartAlarmService;

    @AndroidFindBy(xpath = "//android.widget.Toast[1]")
    public MobileElement toastMsgAlarmService;

    public DemoAndroidPage() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver()), this);
    }

    public void acessaAlarm(){
        Verifications.verifyElementIsClickable(btnApp);
        Report.takeScreenShot();
        btnApp.click();
    }

    public void acessaConfiguracaoAlarm(){
        Verifications.verifyElementIsClickable(btnAlarm);
        btnAlarm.click();
        Report.takeScreenShot();
        Verifications.verifyElementIsClickable(btnAlarmController);
    }

    public void acessaAlarmService(){
        Verifications.verifyElementIsClickable(btnAlarmService);
        btnAlarmService.click();
    }

    public void startAlarmService(){
        Verifications.verifyElementIsClickable(btnStartAlarmService);
        Report.takeScreenShot();
        btnStartAlarmService.click();
    }

    public String validarMensagemServiceAlarm(){
        String msg = getDriver().getPageSource();
        Report.takeScreenShot();
        return msg;
    }
}
