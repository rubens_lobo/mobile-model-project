package steps;

import org.junit.Assert;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.DemoAndroidPage;
import pages.DemoIosPage;
import support.data.DataYaml;

import java.util.Map;

public class IosSteps {

    DemoIosPage demoPage = new DemoIosPage();
    Map data;


    @Quando("eu somar dois valores")
    public void euSomarDoisValores() {
        data = DataYaml.getMapYamlValues("ApiDemoPage", "valores_validos");
        demoPage.somar((String) data.get("valor1"), (String) data.get("valor2"));
    }

    @Entao("o calculo deve retorna a soma")
    public void oCalculoDeveRetornaASoma() {
        Assert.assertEquals("4", demoPage.verificaSoma());
    }

}
