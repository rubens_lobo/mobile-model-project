package steps;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.junit.Assert;
import pages.AlarmPage;
import pages.DemoAndroidPage;
import pages.DemoIosPage;
import support.data.DataYaml;
import support.driver.ContextManager;

import java.util.Map;

public class AdroidSteps {

    DemoAndroidPage demoAndroidPage;

    AlarmPage alarmPage;
    Map data;

    public AdroidSteps() {
        demoAndroidPage = new DemoAndroidPage();
        alarmPage = new AlarmPage();
    }

    @Dado("eu acesso o aplicativo")
    public void euAcessoOAplicativo() {
    }

    @Quando("eu acesso o servico do alarme")
    public void euAcessoOServicoDoAlarm() {
        demoAndroidPage.acessaAlarm();
        demoAndroidPage.acessaConfiguracaoAlarm();
        demoAndroidPage.acessaAlarmService();
    }

    @E("ativo o servico do alarme")
    public void ativoOServicoDoAlarm() {
        demoAndroidPage.startAlarmService();
    }

    @Entao("deve ser ativado o alarme para despertar a cada quinze segundos")
    public void deveSerAtivadoOAlarmeParaDespertarACadaQuinzeSegundos() {
        String toast = DataYaml.getMapYamlValues("ApiDemoPage","alarm_start_service").get("msgToats");
        Assert.assertTrue(demoAndroidPage.validarMensagemServiceAlarm().contains(toast));
    }

    @Quando("eu acesso o controle do alarme")
    public void euAcessoOControleDoAlarme() {
        alarmPage.acessaOpcaoApp();
    }

    @E("ativo o controle {string}")
    public void ativoOControle(String arg0) {
        alarmPage.acessaOpcaoAlarm();
        alarmPage.acessaOpcaoAlarmController();
        alarmPage.clickOneShot();
    }

    @Entao("deve ser ativado o controle {string}")
    public void deveSerAtivadoOControle(String arg0) {
        alarmPage.ckeckToastMsg("One-shot alarm will go off in 30 seconds based on the real time clock. Try changing the current time before then!");
    }
}
