package hooks;

import com.google.common.collect.ImmutableMap;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.touch.TouchActions;
import support.driver.ContextManager;
import support.driver.AppiumDriverSession;
import lombok.extern.log4j.Log4j2;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


@Log4j2
public class Hook extends ContextManager{

    @Before
    public void init(Scenario scenario) {
        log.info(String.format("TESTE INICIADO: %s", scenario.getName()));
        setPlatform(System.getProperty("platform"));
        setScenario(scenario);
        AppiumDriverSession appiumDriverManager = new AppiumDriverSession();
        appiumDriverManager.createInstance();
    }

    @After
    public void end() {
        quit();
        log.info(String.format("TESTE FINALIZADO: %s", getScenario().getName()));
        log.info(String.format("TESTE STATUS: %s", getScenario().getStatus()));
    }
}









