# language: pt
# charset: UTF-8

Funcionalidade: Demo
  Eu como Analista de Qualidade
  GOSTARIA de automatizar os testes
  PARA melhorar a eficiência do controle de qualidade

  # Supported severity values: blocker, critical, normal, minor, trivial. ex.: @severity=critical
  # Every Feature or Scenario can be annotated by following tags: @flaky, @muted, @known

  @android @severity=trivial @muted
  Cenario: Ativar serviço do alarme
    Dado eu acesso o aplicativo
    Quando eu acesso o servico do alarme
    E ativo o servico do alarme
    Entao deve ser ativado o alarme para despertar a cada quinze segundos

  @android
  Cenario: Configuaração do alarme "One Shot Alarm"
    Dado eu acesso o aplicativo
    Quando eu acesso o controle do alarme
    E ativo o controle "One Shot Alarm"
    Entao deve ser ativado o controle "One Shot Alarm"

  @ios
  Cenario: Somar dois valores
    Dado eu acesso o aplicativo
    Quando eu somar dois valores
    Entao o calculo deve retorna a soma