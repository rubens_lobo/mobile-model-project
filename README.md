# PROJETO MODELO PARA TESTE MOBILE 

Projeto desenvolvido com proposito de ser um modelo base para teste para mobile (Android e IOS)

## PRÉ-REQUISITOS

Requisitos de software e hardware necessários para executar este projeto de automação

*   Java 1.8 SDK
*   Maven 3.5.*
*   Intellij IDE
*   Appium (Configuração padrão para Android e IOS)
*   Plugins do Intellij
    * Cumcuber for java
    * Lombok
    * Ideolog 

## ESTRUTURA DO PROJETO

| Diretório                    	| finalidade       	                                                                                        | 
|------------------------------	|---------------------------------------------------------------------------------------------------------- |
| src\main\java\support\config 			| Interface com as propriedades dos arquivos de ambiente 'Properties'                                       |
| src\main\java\support\data    		| Reponsável por ler arquivos yaml file e retonar objeto HashMap com os valores dos campos                  |
| src\main\java\support\dates 			| Metodos de suporte para trabalhar com datas                                                              	|
| src\main\java\support\driver 			| Responsável por fabricar os drivers para os tipos de sistema operacional                    	|
| src\main\java\pages			| Local onde deve ser criado as pages objects para facilitar a manutenção do projeto                       	|
| src\main\java\support\report			| Metodo responsável pela criação de screenshot anexada no Report Alure                                		|
| src\main\java\support			| Metodos de suporte a interação com os elementos web fazendo ações de click e esperas explicitas          	|
| src\main\resources\app	    | Armazenamento dos aplicativos android e ios que será usado pelo script                                                      	|
| src\main\resources\conf	    | Arquivos de configuração globais                                                       	|
| src\main\resources\platform	| Configuraçao dos capabilities necessários para setup do Appium Driver                                                       	|
| src\test\java\hooks          	| Metodos que executam antes e depois de cada teste (@Before, @After)                                   	|
| src\test\java\runner         	| Metodo prinicipal que inicia os testes via cucumber                                                      	|
| src\test\java\steps         	| Local onde deve ser criado as classes que representam os steps definition do cucumber                    	|
| src\test\resources\support\data      	| Massa de dados segregada por ambiente, escritos em arquivos yaml                                      	|
| src\test\resources\features 	| Funcionalidade e cenários de teste escritos em linguagem DSL (Gherkin language)                        	|   
    
## DOWNLOAD DO PROJETO TEMPLATE PARA SUA MÁQUINA LOCAL

Faça o donwload do template no repositório de código para utilizar no seu projeto em especifico, 
feito isso, fique a vontande para usufruir dos recursos disponíveis e 
também customizar de acordo com sua necessidade. 


## FRAMEWORKS UTILIZADOS

Abaixo está a lista de frameworks utilizados nesse projeto

* Jackson - Responsável pela leitura de dados de arquivo yaml file
* Appium - Responsável pela interação com os aplicativos mobile
* Allure - report em HTML
* Java Faker - Geracão de dados sintéticos
* Cucumber - Responsável pela especificação executável de cenários
* Cucumber Junit - Responsável por validar as condições de teste
* Lombok - Otimização de classes modelos
* Log4j2 - Responsável pelo Log do projeto
* AeonBits - Responsável por gerenciar as properties

## COMANDO PARA EXECUTAR OS TESTES

Com o prompt de comando acesse a pasta do projeto, onde esta localizado o arquivo pom.xml, execute o comando abaixo para rodar os testes automatizados.

* Android
```
mvn clean test -Denv=des -Dplatform=android -Dmode=local allure:report
```

* IOS
```
mvn clean test -Denv=des -Dplatform=ios -Dmode=local allure:report
```

## COMANDO PARA GERAR EVIDÊNCIAS EM HTML (ALLURE)

Com o prompt de comando acesse a pasta do projeto, onde esta localizado o arquivo pom.xml, execute o comando abaixo para gerar as evidências em HTML

Gera o report
```
mvn allure:report
```

Gera o report e abri no navegador automaticamente
```
mvn allure:serve
```


## MULTIPLOS COMANDOS 

Você também pode mesclar a linha de comando maven com options do cucumber, 
sendo assim você pode escolher uma determinada tag que se deseja executar do cucumber, 
podendo escolher também a massa de dados que irá utilizar e juntamente aplicar o linha de comando para gerar o report HTML.

```
mvn clean test -Dcucumber.options="--tags @dev" -Denv=des -Dplatform=android -Dmode=local allure:report
```

### Detalhes dos comandos

| Comando                    	| finalidade       	                                                                                        | 
|------------------------------	|---------------------------------------------------------------------------------------------------------- |
| -Dcucumber.options="--tags @dev"| Sobrescreve os parametros do cucumber, neste exemplo estou filtrado os teste pela tag @dev, então somente os cenários com esta tag irão executar                 |
| -Denv=des   		| Seleciona qual massa de teste de acordo com o ambiente que vai ser utilizado, exemplo: des, prod ou uat              |
| -Dplatform=android| Seleciona qual plataforma vai ser executado os teste, exemplo: android ou ios             |
| -Dmode=local 		| Seleciona qual o modo de execução (local ou remoto) o default é local, mas você pode customizar e criar configurações de capabilities para rodar de forma remota como BrowserStack |


## PIPELINE - TESTES CONTINUOS 

Executar testes de forma continua vem se tornado fundamental para agregar valor junto a seu time,
para isto foi configurado o pipeline para ser aplicado ao BitBucket chamando "bitbucket-pipelines.yml"
localizado na raiz do projeto

```yaml
#  Template maven-build

#  This template allows you to test and build your Java project with Maven.
#  The workflow allows running tests, code checkstyle and security scans on the default branch.

# Prerequisites: pom.xml and appropriate project structure should exist in the repository.

image: maven:3.5.3-jdk-8-alpine

pipelines:
  default:
    - parallel:
        - step:
            name: Build and Test
            caches:
              - maven
            script:
              - mvn clean test -Dcucumber.options="--tags '@android'" -Denv=des -Dplatform=android -Dmode=remote
            after-script:
              # Collect checkstyle results, if any, and convert to Bitbucket Code Insights.
              - pipe: atlassian/checkstyle-report:0.2.0
        - step:
            name: Security Scan
            script:
              # Run a security scan for sensitive data.
              # See more security tools at https://bitbucket.org/product/features/pipelines/integrations?&category=security
              - pipe: atlassian/git-secrets-scan:0.4.3
```



## EVIDÊNCIAS

Os arquivos com as evidências ficam localizados na pasta target do projeto, esta pasta só é criada depois da primeira execução.

```
 Report HTML: target\site\index.html
 Json Cucumber: target\json-cucumber-reports\cucumber.json
 Xml Junit: tagert\xml-junit\junit.xml
```
Ps.: Caso você necessite utilizar do Allure, o mesmo somente cria os relatórios pelo maven com o paramêtro 'allure:report', conforme exemplo de múltiplos comandos acima.

## LOG DE EXECUÇÃO

Os logs de execução gerados pelo Log4j2 ficam alocados na pasta target/log

## ADB COMANDOS

Verificar se o adb está conectado a um dispositivo

```
adb devices
```

Execute o seguinte comando em cmd para recuperar o pacote do aplicativo e o arquivo principal

```
adb shell "dumpsys window | grep -E ',CurrentFocus|mFocusedApp'"
```

instalar de aplicativos via adb

```
adb install <PATH>.apk
```

Desinstalar de aplicativos via adb

```
adb uninstall <package_name>
```

## LINKS DE APOIO

* [Instruções de configuração do ambiente de desenvolvimento para automação android](https://eliasnogueira.github.io/appium-workshop/)
  - <b>Atenção</b>
    - Substutiuir o Eclipse pelo Intellij
    - Usar esse template para o desenvolvimento dos testes automatizados

* [Appium Oficial](https://appium.io/)
